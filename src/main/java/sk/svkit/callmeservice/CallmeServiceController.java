package sk.svkit.callmeservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class CallmeServiceController {

    private AtomicInteger counter = new AtomicInteger();

    @GetMapping("/stats")
    public Integer numberOfCalls() {
        return counter.getAndIncrement();
    }
}
